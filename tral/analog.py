import librosa as R
import librosa.display as RD
import librosa.feature as RF
import numpy as N
import matplotlib.pyplot as pyplot
import IPython
from .const import *

def load(fpath):
    samples,_ = R.load(fpath, sr=SAMPLE_RATE)
    return samples

def play_audio(x):
    if type(x) == str:
        return IPython.display.Audio(filename=x)
    else:
        return IPython.display.Audio(data=x, rate=SAMPLE_RATE)

def cqt(samples):
    return R.core.cqt(samples, sr=SAMPLE_RATE, n_bins=PITCH_MAX, fmin=R.note_to_hz(NOTE_MIN), bins_per_octave=12, hop_length=HOP_LENGTH).transpose()

def chroma_cqt(samples):
    return RF.chroma_cqt(samples, sr=SAMPLE_RATE, n_chroma=120, fmin=R.note_to_hz(NOTE_MIN), bins_per_octave=12, hop_length=HOP_LENGTH).transpose()

def plot_cqt(spectrum, \
             is_complex=False, frequency_axis_last=True, \
             in_db=False, time_shift=-WINDOW_LEFT / FRAME_RATE, window_shift=None, show_colorbar=True, \
             horizontal_lines=None):
    if is_complex:
        spectrum = N.abs(spectrum)
    if frequency_axis_last:
        spectrum = spectrum.transpose()
    if not in_db:
        spectrum = R.amplitude_to_db(spectrum, ref=N.max)
        
        
    if window_shift is not None:
        time_shift = window_shift - WINDOW_LEFT / FRAME_RATE
        
    x_grid = N.array(range(spectrum.shape[1]+1),dtype='float') * HOP_LENGTH / SAMPLE_RATE + time_shift
    RD.specshow(spectrum, x_axis='time', y_axis='cqt_note', \
        x_coords=x_grid, \
        hop_length=HOP_LENGTH, sr = SAMPLE_RATE, \
        fmin=R.note_to_hz(NOTE_MIN))
    
    if horizontal_lines:
        for f in horizontal_lines:
            pyplot.axhline(R.midi_to_hz(f))
    
    if show_colorbar:
        pyplot.colorbar(format='%+2.0f dB')
        
        
def frame_to_second(t):
    return t * HOP_LENGTH / SAMPLE_RATE
def second_to_frame(t):
    return int(round(t * FRAME_RATE))

def spectrum_window(spectrum, time, is_frame=False):
    assert spectrum.shape[1] == PITCH_MAX
    index_min = 0
    index_max = spectrum.shape[0]
    if is_frame:
        index = time
    else:
        index = second_to_frame(time)
    index_left = index - WINDOW_LEFT
    index_right = index + WINDOW_RIGHT
    
    result = spectrum[max(index_min,index_left):min(index_max,index_right),:]
    left_pad = max(0, index_min - index_left)
    right_pad = max(0, index_right - index_max)
    if left_pad > 0 or right_pad > 0:
        result = N.pad(result, ((left_pad, right_pad), (0,0)), constant_values=0)
    
    #print("{}, {}, {}, {}, {}".format(index_min, index_left, index_right, index_max, result.shape))
    assert result.shape[0] == WINDOW_WIDTH
    return result

