import numpy as N
import tensorflow.keras.utils as KU
import sklearn.utils as SU
import random
import scipy.io

from .const import *
from .metrics import *

class DataGenMeta(KU.Sequence):

    def __init__(self, filenames, radius=5, \
                 diff_time=False, flatten=True):
        """
        diff_time: If true, compute time differentials w.r.t. adjacent notes instead of w.r.t. a central note.
        """
        self.filenames = filenames
        self.radius = radius
        self.diff_time = diff_time
        self.flatten = flatten
        
        
    def all_samples(self):
        def file_to_slice(f):
            notes = DataGenMeta.read_meta(f)
            return self.generate_slices(notes)
        
        result = [file_to_slice(f) for f in self.filenames]
        slic_x, slic_y = zip(*result)
        
        slic_x = N.concatenate(slic_x, axis=0)
        slic_y = N.concatenate(slic_y, axis=0)
        
        self.class_weight = generate_class_weights(slic_y)
        slic_w = SU.class_weight.compute_sample_weight(self.class_weight, slic_y)
        
        return slic_x, slic_y, slic_w
    
        
    @staticmethod
    def read_meta(fpath):
        fpath += ".mid.mat"
        mat = scipy.io.loadmat(fpath)
        notes = mat['notes']
        assert len(notes.shape) == 2
        return notes
    
    def generate_single_slice(self, li, i):
        assert self.radius > 0
        assert i in range(len(li))
        
        front_pad = max(0, self.radius - i)
        back_pad = max(0, i + self.radius - (len(li) - 1))
        time_offset = li[i][0]
            
        slic_front = N.array(li[max(0, i-self.radius):i])
        slic_back = N.array(li[i+1:i+self.radius+1])
        
        if self.diff_time:
            slic_front[:,0] = N.diff(slic_front[:,0], append=time_offset)
            slic_back[:,0]  = N.diff(slic_back[:,0],prepend=time_offset)
        else:
            slic_front[:,0] -= time_offset
            slic_back[:,0] -= time_offset
            slic_front[:,0] *= -1
        
        slic = N.concatenate([slic_front, slic_back])
        
        # Remove the note duration column
        slic = slic[:,[0,2]]
        slic_y = li[i][1]
        assert len(slic.shape) == 2
        assert slic.shape[-1] == 2
        
        # re-centre the notes relative to li[i][-1]
        pitch_offset = li[i][-1]
            
        assert pitch_offset in range(PITCH_MAX)
        
        #slic[:,0] = N.abs(slic[:,0])
        slic[:,1] -= pitch_offset
        
        PAD_ELEMENT = N.array([[-1, -1]])
        if front_pad > 0 or back_pad > 0:
            slic = [
                N.repeat(PAD_ELEMENT, front_pad, 0),
                slic,
                N.repeat(PAD_ELEMENT, back_pad, 0),
            ]
            slic = N.concatenate(slic, axis=0)
            
        assert slic.shape == (2*self.radius, 2)
        if self.flatten:
            slic = slic.flatten()
            assert len(slic.shape) == 1
        return slic, slic_y
            
    
    def generate_slices(self, li):
        result = [self.generate_single_slice(li, i) for i in range(len(li))]
        slic_x, slic_y = zip(*result)
        
        slic_x = N.concatenate([a[N.newaxis,:] for a in slic_x], axis=0)
        slic_y = N.array(slic_y)
        
        #print("{}, {}".format(len(li), slic_x.shape))
        assert slic_x.shape == (len(li), 2 * self.radius * 2)
        assert slic_y.shape == (len(li),)
        
        return slic_x, slic_y
    