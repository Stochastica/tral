import numpy as N
import numpy.random as NR
import tensorflow.keras.utils as KU
import mido
import scipy.io
import bisect
import random
import itertools
from expiringdict import ExpiringDict

from .analog import *
from .midi import *
from .const import *
from .instruments import FUNDAMENTAL_INSTRUMENTS, NAMES as INSTRUMENT_NAMES

def extract_activation_slices(spectrum, track, ticks_per_beat):
    """
    Let K be the number of activations in the given track.
    
    Produce a (K,WINDOW_WIDTH,PITCH_MAX) tensor, with each (1,_,_) slice being a cqt window focused at the beginning of one or several notes.
    
    track: Either a list of activation times or a mido track
    """
    if type(track) == mido.MidiTrack:
        track = track_activation_times(track, ticks_per_beat)
        track = [x[0] for x in track]
        track = sorted(set(track))
        track = [t / ticks_per_beat * TEMPO / 1000000 for t in track]
        
    # stack up the slices
    slices = [spectrum_window(spectrum, t) for t in track]
    slices = [x[N.newaxis,:] for x in slices]
    
    return N.concatenate(slices,axis=0),track
    
def extract_activation_slices_file(fpath, voice):
    """
    Convention:
    fpath + ".v{voice}.mat" is a mat file of the spectrum
    fpath + ".mid.mat" is a mat file for the notes
    """
    fpath_spec = fpath + ".v{}.mat".format(voice)
    fpath_notes = fpath + ".mid.mat"
    
    spectrum = scipy.io.loadmat(fpath_spec)['spectrum']
    notes = scipy.io.loadmat(fpath_notes)
    activations = N.unique(notes['notes'][:,0])
    tpb = notes['tpb']
    
    return extract_activation_slices(spectrum, activations, tpb)
    
 
def spectrum_regularise(spec):
    return (spec / 80) + 1
def spectrum_unregularise(spec):
    return (spec - 1) * 80

class DataGenTranscriber(KU.Sequence):
    
    def __init__(self, \
                 filenames, \
                 range_voice=FUNDAMENTAL_INSTRUMENTS, \
                 batchsize=2, \
                 voice_embed_func=lambda x:N.array([x,0]), \
                 mask = MASK_VAL, \
                 frame_based_time = True, \
                 vocab_size=None, \
                 seed=None, \
                 randomise_index=False, \
                 use_recombiantion=False, \
                 cache_size=500):
        self.filenames = filenames
        self.range_voice = range_voice
        self.batchsize = batchsize
        self.voice_embed_func = voice_embed_func
        self.mask = mask
        self.vocab_size = vocab_size
        self.frame_based_time = frame_based_time
        self.cache = ExpiringDict(max_len=cache_size, max_age_seconds=1200)
        self.use_recombiantion = use_recombiantion
        self._read_activations(seed, randomise_index)
        
    def _read_activations(self, seed, randomise_index):
        def read_single(f):
            return scipy.io.loadmat(f + ".mid.mat")
        
        self.activations = [read_single(f) for f in self.filenames]
        
        def length_func(x):
            return x["times"].shape[1]
        self.act_cdf = [length_func(x) for x in self.activations]
        self.act_cdf = list(N.add.accumulate(self.act_cdf))
        
        assert len(self.act_cdf) == len(self.activations)
        assert len(self.activations) == len(self.filenames)
        self.act_n = int(self.act_cdf[-1])
        
        self.index_list = range(self.len_activations())
        
        random.seed(seed)
        self.recombination_list = list(self.index_list)
        
        if randomise_index:
            self.index_list = list(self.index_list)
            random.shuffle(self.index_list)
            
        random.shuffle(self.recombination_list)
        self.recombination_voices = random.choices(range(1, len(self.range_voice)), k=len(self.recombination_list))
        
        
    def len_activations(self):
        return self.act_n * len(self.range_voice)
    def __len__(self):
        def int_ceildiv(a: int, b: int):
            return -((-a) // b)
        return int_ceildiv(self.len_activations(), self.batchsize)
        #return (self.len_activations() + self.batchsize - 1) // self.batchsize
        
    def _get_indices(self, index):
        voice_index = index % len(self.range_voice)
        index = index // len(self.range_voice)
        file_index = bisect.bisect_right(self.act_cdf, index)
        if file_index > 0:
            slice_index = index - self.act_cdf[file_index-1]
        else:
            slice_index = index
        assert voice_index in range(len(self.range_voice))
        assert file_index in range(len(self.filenames))
        
        return voice_index, file_index, slice_index
    
    def _load_mat(self, fpath):
        if fpath in self.cache:
            return self.cache.get(fpath)
        else:
            result = scipy.io.loadmat(fpath)['spectrum']
            self.cache[fpath] = result
            return result
    
    def generate_one(self, index, verbose=False):
        
        voice_index, file_index, slice_index = self._get_indices(index)
        if verbose:
            print("{}, {}".format(file_index, slice_index))
        
        voice_index2 = self.recombination_voices[index]
        _, file_index2, slice_index2 = self._get_indices(self.recombination_list[index])
        
        assert voice_index2 != 0
            
        voice = self.range_voice[voice_index]
        voice2 = self.range_voice[(voice_index + voice_index2) % len(self.range_voice)]
        
        
        act = self.activations[file_index]
        act2 = self.activations[file_index2]
        # choose random activation slice
        n_slices = act['times'].shape[1]
        
        assert slice_index in range(n_slices)
        
        time = act['times'][0,slice_index]
        time2 = act2['times'][0,slice_index2]
        
        if verbose:
            print("Slice time: {}".format(time))
            
        fpath_mat = self.filenames[file_index] + '.v{}.mat'.format(voice)
        fpath_mat2 = self.filenames[file_index2] + '.v{}.mat'.format(voice2)
            
        slic = N.zeros((WINDOW_WIDTH, PITCH_MAX), dtype='complex128')
        slic += spectrum_window(self._load_mat(fpath_mat), time, is_frame=self.frame_based_time)
        if self.use_recombiantion:
            slic += spectrum_window(self._load_mat(fpath_mat2), time2, is_frame=self.frame_based_time)
        slic = N.abs(slic)
        slic += NR.normal(loc=0.0, scale=0.0005, size=slic.shape)
        slic = R.amplitude_to_db(slic, ref=N.max)
        slic = spectrum_regularise(slic)
        
        
        
        words = act['words'][slice_index,:]
        (words_len,) = N.where(words >= 0)
        words_len = words_len[-1] + 1
        words_in = N.pad(words[:words_len - 1], ((1,0),), 'constant', constant_values=0)
        words = words[:words_len]
        
        assert len(slic.shape) == 2
        assert len(words_in.shape) == 1
        assert len(words.shape) == 1
        return self.voice_embed_func(voice), slic, words_in, words
    
    
    def plot_sample(self, i, notewise, draw_lines=False):
        voice, _, _ = self._get_indices(i)
        voice = self.range_voice[voice]
        _, slic, words_in, words = self.generate_one(i, verbose=True)
        print("Voice={} ({})".format(voice, INSTRUMENT_NAMES[voice]))
        if notewise:
            print("{} -> {}".format(notewise.to_string(words_in), notewise.to_string(words)))
        slic = spectrum_unregularise(slic)
        if draw_lines:
            on_notes = [notewise.decode(x) for x in words if x >= 0]
            on_notes = [n for x,n in on_notes if x == 'on']
            plot_cqt(slic, in_db=True, horizontal_lines=on_notes)
        else:
            plot_cqt(slic, in_db=True)
    
    
    def __getitem__(self, idx):
        #print("Begin index: {} (len = {})".format(idx * self.batchsize, self.len_activations()))
        samples = [self.index_list[idx * self.batchsize + i] \
                   for i in range(self.batchsize) \
                   if idx * self.batchsize + i < self.len_activations()]
        
        samples = [self.generate_one(i) for i in samples]
        assert len(samples) > 0
        li_v, li_s, li_win, li_wout = zip(*samples)
        voices_in = N.concatenate([v[N.newaxis,:] for v in li_v])
        slices_in = N.concatenate([s[N.newaxis,:] for s in li_s])
        words_in = self.justified_merge([w[N.newaxis,:] for w in li_win])
        words_out = self.justified_merge([w[N.newaxis,:] for w in li_wout])
        
        #if self.vocab_size:
        #    words_in = KU.to_categorical(words_in, num_classes=self.vocab_size)
        if self.vocab_size:
            # Keras to categorical does not work well on shapes such as (8,1).
            # In which case the dimension with size 1 will be "erased", so we have to add
            # the dimension back in.
            if words_out.shape[1] == 1:
                words_out = words_out[:,N.newaxis]
            words_out = KU.to_categorical(words_out, num_classes=self.vocab_size)
            assert len(words_out.shape) == 3
        
        return [voices_in, slices_in, words_in], words_out
    
    
    def justified_merge(self, li, truncate=None):
        if not truncate:
            truncate = max([x.shape[1] for x in li])
        
        def padone(x):
            if truncate > x.shape[1]:
                pad = truncate - x.shape[1]
                return N.pad(x, ((0,0),(0,pad)), 'constant', constant_values=self.mask)
            elif truncate < x.shape[1]:
                return x[:,:truncate,:]
            else:
                return x
        
        result = N.concatenate([padone(x) for x in li])
        assert result.shape[1] == truncate
        return result
    
    
class DataGenTranscriberTrainer(KU.Sequence):
    
    def __init__(self, \
                 basepath = 'mat/single-inst/', \
                 range_voice=FUNDAMENTAL_INSTRUMENTS, \
                 range_pitch = range(21,109), \
                 batchsize=2, \
                 voice_embed_func=lambda x:N.array([x,0]), \
                 mask = MASK_VAL, \
                 notewise = None, \
                 seed = 0, \
                 n_samples = 12):
        self.range_voice = range_voice
        self.range_pitch = range_pitch
        self.batchsize = batchsize
        self.voice_embed_func = voice_embed_func
        self.mask = mask
        self.notewise = notewise
        self.n_samples = n_samples
        
        self._read_notes(basepath)
        self._generate_samples(seed)
        
    def _read_notes(self, basepath):
        self.cache = dict()
        for p,v in itertools.product(self.range_pitch, self.range_voice):
            spectrum = scipy.io.loadmat(basepath + "sample.p{}v{}.mat".format(p,v))
            spectrum = spectrum['spectrum']
            self.cache[(p,v)] = spectrum
            
    def _generate_samples(self, seed):
        self.samples = []
        NR.seed(seed)
        for i in range(self.n_samples):
            [voice_main, voice_noise] = NR.choice(self.range_voice, size=2, replace=False).tolist()
            rest = NR.choice(DURATION_LIST)
            n_notes_ontime = NR.choice(range(0,5))
            notes_ontime = sorted(NR.choice(self.range_pitch, size=n_notes_ontime, replace=False).tolist())
            
            n_notes_offtime = NR.choice(range(2,5))
            notes_offtime = sorted(NR.choice(self.range_pitch, size=n_notes_offtime, replace=False).tolist())
            
            n_notes_noise = NR.choice(range(2,5))
            notes_noise = sorted(NR.choice(self.range_pitch, size=n_notes_noise, replace=False).tolist())
            
            sample = (voice_main, voice_noise, rest, notes_ontime, notes_offtime, notes_noise)
            self.samples.append(sample)
            
        assert len(self.samples) == self.n_samples
            
    def get_sample(self, i):
        voice_main, voice_noise, rest, notes_ontime, notes_offtime, notes_noise = self.samples[i]
        
        # The notes in the .mat files are delayed by a whole beat
        mat_time_shift = TEMPO / 1000000
        slic = N.zeros((WINDOW_WIDTH, PITCH_MAX), dtype='complex128')
        for p in notes_ontime:
            spec_note = self.cache[(p, voice_main)]
            spec_note = spectrum_window(spec_note, mat_time_shift)
            slic += spec_note
        for p in notes_noise:
            spec_note = self.cache[(p, voice_noise)]
            spec_note = spectrum_window(spec_note, mat_time_shift)
            slic += spec_note
        for p in notes_offtime:
            spec_note = self.cache[(p, voice_main)]
            spec_note = spectrum_window(spec_note, mat_time_shift - rest / 32 * mat_time_shift)
            slic += spec_note
        
        slic += NR.normal(loc=0.0, scale=0.0005, size=slic.shape)
        words = [self.notewise.e_note_on(x) for x in notes_ontime] + [self.notewise.e_rest(rest)]
        words_in = [self.notewise.get_startsymbol()] + words[:-1]
        
        slic = N.abs(slic)
        slic = R.amplitude_to_db(slic, ref=N.max)
        slic = spectrum_regularise(slic)
        
        return self.voice_embed_func(voice_main), slic, words_in, words
    
    def plot_sample(self, i, draw_lines=False):
        voice_main, voice_noise, rest, notes_ontime, notes_offtime, notes_noise = self.samples[i]
        print("Main={}, Noise={}, Rest={}".format(voice_main, voice_noise, rest))
        print("Ontime={}, Offtime={}, Noise={}".format(notes_ontime, notes_offtime, notes_noise))
        _, slic, words_in, words = self.get_sample(i)
        print("{} -> {}".format(self.notewise.to_string(words_in), self.notewise.to_string(words)))
        slic = spectrum_unregularise(slic)
        if draw_lines:
            plot_cqt(slic, in_db=True, horizontal_lines=notes_ontime)
        else:
            plot_cqt(slic, in_db=True)
    
    def __getitem__(self, idx):
        samples = [self.get_sample(i) \
                   for i in range(self.batchsize) \
                   if idx * self.batchsize + i < len(self.samples)]
        assert len(samples) > 0
        voices_in, slices_in, words_in, words = zip(*samples)
        
        voices_in = N.concatenate([v[N.newaxis,:] for v in voices_in])
        slices_in = [N.abs(s)[N.newaxis,:] for s in slices_in]
        slices_in = N.concatenate(slices_in)
        words_in = self.justified_merge([N.array(w)[N.newaxis,:] for w in words_in])
        words = self.justified_merge([N.array(w)[N.newaxis,:] for w in words])
        
        words = KU.to_categorical(words, num_classes=self.notewise.length_reduced)
        
        return [voices_in, slices_in, words_in], words
            
            
    def __len__(self):
        def int_ceildiv(a: int, b: int):
            return -((-a) // b)
        return int_ceildiv(len(self.samples), self.batchsize)
    
    
    def justified_merge(self, li, truncate=None):
        if not truncate:
            truncate = max([x.shape[1] for x in li])
        
        def padone(x):
            if truncate > x.shape[1]:
                pad = truncate - x.shape[1]
                return N.pad(x, ((0,0),(0,pad)), 'constant', constant_values=self.mask)
            elif truncate < x.shape[1]:
                return x[:,:truncate,:]
            else:
                return x
        
        result = N.concatenate([padone(x) for x in li])
        assert result.shape[1] == truncate
        return result
    
    

class DataGenTranscriberNonsequential(KU.Sequence):
    
    def __init__(self, \
                 filenames, \
                 instruments=FUNDAMENTAL_INSTRUMENTS, \
                 batchsize=16, \
                 voice_embed_func=lambda x:N.array([x,0]), \
                 mask = MASK_VAL, \
                 frame_based_time = True, \
                 notewise=None, \
                 seed=0):
        self.filenames = filenames
        self.instruments = instruments
        self.batchsize = batchsize
        self.voice_embed_func = voice_embed_func
        self.mask = mask
        self.notewise = notewise
        self.frame_based_time = frame_based_time
        
        self._read_activations(seed)
        
    def _read_activations(self, seed):
        def read_single(f):
            return scipy.io.loadmat(f + ".mid.mat")
        
        self.activations = [read_single(f) for f in self.filenames]
        
        def length_func(x):
            return x["times"].shape[1]
        self.act_cdf = [length_func(x) for x in self.activations]
        self.act_cdf = list(N.add.accumulate(self.act_cdf))
        
        assert len(self.act_cdf) == len(self.activations)
        assert len(self.activations) == len(self.filenames)
        self.act_n = int(self.act_cdf[-1])
        
        random.seed(seed)
        self.index_list = list(range(self.len_activations()))
        random.shuffle(self.index_list)
        
    def len_activations(self):
        return self.act_n * len(self.instruments)
    def __len__(self):
        def int_ceildiv(a: int, b: int):
            return -((-a) // b)
        return int_ceildiv(self.len_activations(), self.batchsize)
        #return (self.len_activations() + self.batchsize - 1) // self.batchsize
    
    def generate_one(self, index, verbose=False):
        voice_index = index % len(self.instruments)
        index = index // len(self.instruments)
        file_index = bisect.bisect_right(self.act_cdf, index)
        
        if file_index > 0:
            slice_index = index - self.act_cdf[file_index-1]
        else:
            slice_index = index
           
        assert voice_index in range(len(self.instruments))
        assert file_index in range(len(self.filenames))
            
        # choose random instrument
        voice = self.instruments[voice_index]
        # chooce random file
        fpath_mat = self.filenames[file_index] + '.v{}.mat'.format(voice)
        
        act = self.activations[file_index]
        # choose random activation slice
        n_slices = act['times'].shape[1]
        
        if verbose:
            print("{}, {}, {}".format(file_index, slice_index, n_slices))
        #if file_index != len(self.filenames) - 1:
        #    assert n_slices == self.act_cdf[file_index+1] - self.act_cdf[file_index]
        assert slice_index in range(n_slices)
        
        time = act['times'][0,slice_index]
        words = act['words'][slice_index,:]
        
        if verbose:
            print("Slice time: {}".format(time))
        
        if fpath_mat in self.cache:
            spectrum = self.cache.get(fpath_mat)
        else:
            spectrum = scipy.io.loadmat(fpath_mat)['spectrum']
            self.cache[fpath_mat] = spectrum
            
        slic = N.abs(spectrum_window(spectrum, time, is_frame=self.frame_based_time))
        
        slic = R.amplitude_to_db(slic, ref=N.max)
        slic = spectrum_regularise(slic)
        
        words = words[words >= 0]
        rest_length = max(DURATION_LIST)
        has_rest = False
        state = N.zeros((PITCH_MAX,), dtype='int8')
        for x in words:
            ty,val = self.notewise.decode(x)
            if ty == 'on':
                state[val] = 1
            elif ty == 'rest':
                assert not has_rest
                has_rest = True
                rest_length = val
        rest_length = DURATION_LIST.index(rest_length)
        
        assert len(slic.shape) == 2
        assert state.shape == (PITCH_MAX,)
        return self.voice_embed_func(voice), slic, state, rest_length
    
    def __getitem__(self, idx):
        #print("Begin index: {} (len = {})".format(idx * self.batchsize, self.len_activations()))
        samples = [self.index_list[idx * self.batchsize + i] \
                   for i in range(self.batchsize) \
                   if idx * self.batchsize + i < self.len_activations()]
        
        samples = [self.generate_one(i) for i in samples]
        assert len(samples) > 0
        li_v, li_s, li_states, li_rest_length = zip(*samples)
        voices_in = N.concatenate([v[N.newaxis,:] for v in li_v])
        slices_in = N.concatenate([s[N.newaxis,:] for s in li_s])
        states_out = N.concatenate([s[N.newaxis,:] for s in li_states])
        rest_out = N.array(li_rest_length)
        
        return [voices_in, slices_in], [states_out, rest_out]
    
    